<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Container;

Route::get('/', function () {
    $view = view('overview');
    $view->containers = Container::orderBy('current_volume', 'DESC')->get();

    return $view;
});
Auth::routes();

