<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::resource('user', 'UserController');
Route::resource('container', 'ContainerController');

Route::get('token/index', 'ApiTokenController@index')->name('token.index');
Route::get('token/store', 'ApiTokenController@store')->name('token.store');


