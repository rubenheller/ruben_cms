<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# Container aanmaken en opslaan
Route::post('/container/create', function (Request $request) {
    $tokens  = \App\ApiToken::all();

    #bekijkt of api token bestaat in database
    if ($tokens->contains('token', $request->api_token)) {
        $existing_token = \App\ApiToken::where('token', $request->api_token)->first();

        #bekijkt of de meegestuurde api key niet al wordt gebruikt in database
        if($existing_token->container()->exists()){
            return ['message' => 'Api token is already in use'];
        }

        #container wordt toegevoegd met bijbehoorende api_token_id voor relatie
        $request['api_token_id'] = $existing_token->id;
        $container = \App\Container::create($request->only(['api_token_id', 'latitude', 'longitude']));
        return ['id' => $container->id, 'message' => 'Container succesfully stored with id: '];
    }else {
        return ['message' => 'Api token is incorrect'];
    }

});

Route::post('/container/store-data', function (Request $request) {
    $tokens  = \App\ApiToken::all();

    #bekijkt of api token bestaat in database
    if($tokens->contains('token', $request->api_token)) {
        $container = \App\Container::find($request->container_id);
        $container->current_volume = $request->volume;
        $container->save();
        return ['message' => 'Container volume updated'];

    }else{
        return ['message' => 'Api token is incorrect'];
    }
});

