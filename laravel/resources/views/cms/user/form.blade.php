<div class="form-group">
    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id'=> 'nameBox', 'placeholder'=>'Naam']) }}
</div>
<div class="form-group">
    {{ Form::text('email', old('email'), ['class' => 'form-control', 'placeholder'=>'Email']) }}
</div>
<div class="form-group">
    <input type="password" name="password" class="form-control" placeholder="Wachtwoord" >
</div>
<div class="form-group">
    <input type="password" name="password_confirmation" class="form-control" placeholder="Wachtwoord bevestigen" >
</div>
