@extends('layouts.app')
@section('content')
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
         style="min-height: 200px; background-size: cover; background-position: center top;">
        <span class="mask bg-gradient-default opacity-8"></span>
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-12 col-md-10">
                    <h1 class="display-2 text-white">{{ $title }}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12">
                @include('layouts.notifications')
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h3 class="mb-0">{{ $title }}</h3>
                            </div>
                            <div class="col-md-4 text-right">
                                <a href="{{ route('admin.user.create') }}" class="btn btn-sm btn-primary">Nieuwe
                                    Administrator</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover align-items-center">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                <tbody class="clickable">
                                @foreach($users as $user)
                                    <tr data-url="{{ route('admin.user.edit', $user->id) }}">
                                        <td width="40%">{{ $user->name }}</td>
                                        <td width="40%">{{ $user->email }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection