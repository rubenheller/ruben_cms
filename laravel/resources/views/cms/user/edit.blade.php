@extends('layouts.app')
@section('content')
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
         style="min-height: 200px; background-size: cover; background-position: center top;">
        <span class="mask bg-gradient-default opacity-8"></span>
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-12 col-md-10">
                    <h1 class="display-2 text-white">{{ $title }}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12">
                @include('layouts.notifications')

                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <h3 class="mb-0">{{ $title }}</h3>
                            </div>
{{--                            <div class="col-6">--}}
{{--                                <button type="button" class="btn btn-sm btn-danger float-right" data-toggle="modal" data-target="#deleteModal">Administrator verwijderen</button>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::model($user,['route' =>['admin.user.update', $user->id], 'method' => 'PUT']) }}
                        {{ Form::hidden('user_id', $user->id) }}
                        @include('cms.user.form')
                        <button type="submit" class="btn btn-sm btn-primary">Wijzigingen opslaan</button>
                        {{ Form::close() }}

                        {{ Form::open(['route' => ['admin.user.destroy' ,$user->id], 'method' => 'DELETE', 'id' => 'delete']) }}
                        {{ Form::close() }}
                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Weet u zeker dat u {{ $user->name }} wil verwijderen?</h5>
                                    </div>
                                    <div class="modal-footer" style="color: white;">
                                        <a class="btn btn-sm btn-success" onclick="$('#delete').submit()">Ja</a>
                                        <a class="btn btn-sm btn-danger" data-dismiss="modal">Nee</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
