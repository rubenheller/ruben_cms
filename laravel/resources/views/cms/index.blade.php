@extends('layouts.app')
@section('content')
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
         style="min-height: 200px; background-size: cover; background-position: center top;">
        <!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid align-items-center">
            <div class="row mb-4">
                <div class="col-lg-3">
                    <div class="card card-stats">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Totaal Aantal Containers:</h5>
                                    <span class="h2 font-weight-bold mb-0">{{ $containers->count() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12">
                <div class="card bg-secondary card-profile shadow mb-5">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3 class="m-0 p-0">Containers</h3>
                            </div>
                            {{--                            <div class="col-lg-6">--}}
                            {{--                                <a href="{{ route('admin.container.create') }}" style="float: right" class="btn btn-sm btn-primary">--}}
                            {{--                                    Container toevoegen--}}
                            {{--                                </a>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                    <div class="table-responsive">
                        <div>
                            <table class="table align-items-center">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">
                                        Coordinaten
                                    </th>
{{--                                    <th scope="col">--}}
{{--                                        Status--}}
{{--                                    </th>--}}
                                    <th scope="col">
                                        Volume (%)
                                    </th>
                                    <th scope="col">
                                        ID
                                    </th>
                                    <th scope="col">
                                        Opties
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                @forelse($containers as $container)
                                <tr class="@if($container->current_volume >= $container->max_volume) bg-red text-white @endif">
                                    <td>
                                        <span class="badge badge-dot mr-4">
                                            {{$container->coordinates}}
                                        </span>
                                    </td>
{{--                                    <td>--}}
{{--                                        <span class="badge badge-dot mr-4">--}}
{{--                                            <i class="bg-warning"></i> pending--}}
{{--                                        </span>--}}
{{--                                    </td>--}}
                                    <td class="completion">
                                        <div class="d-flex align-items-center">
                                            <span class="mr-2">{{$container->current_volume}}%</span>
                                            <div>
                                                <div class="progress">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                         aria-valuenow="60" aria-valuemin="0"
                                                         aria-valuemax="100" style="width: {{$container->current_volume}}%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                            {{ $container->id }}
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-primary"
                                           target="_blank"
                                           href="http://www.google.com/maps/place/{{$container->coordinates}}">
                                            Locatie
                                        </a>
                                        <a class="btn btn-sm btn-primary"
                                           href="{{ route('admin.container.edit', $container->id) }}">Wijzigen
                                        </a>
                                    </td>
                                </tr>
                                    @empty
                                    <tr>
                                        <td>
                                            Geen Containers
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
