<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title }}</title>
    {{--    <link href="{{ asset('favicon.ico') }}" rel="icon" type="image/png">--}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    {!! HTML::style('argon-dashboard-master/assets/vendor/nucleo/css/nucleo.css') !!}
    {!! HTML::style('argon-dashboard-master/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') !!}
    {!! HTML::style('argon-dashboard-master/assets/css/argon.min.css') !!}
    {!! HTML::style('css/app.css') !!}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    @yield('head')
</head>
<body id="app">
{{--@if(Route::currentRouteName() == 'admin.page.edit')--}}
    {{--@include('layouts.page_edit_side')--}}
    {{--@include('templates.default.index')--}}
{{--@else--}}
    @include('layouts.side_nav')
    <div class="main-content">
        @include('layouts.upper_nav')
        @yield('content')
    </div>
{{--@endif--}}
{!! HTML::script('argon-dashboard-master/assets/vendor/jquery/dist/jquery.min.js') !!}
{!! HTML::script('argon-dashboard-master/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') !!}
{!! HTML::script('argon-dashboard-master/assets/js/argon.min.js') !!}
<script>
    $('.clickable tr').click(function () {
        window.location.href = $(this).data('url');
    });
</script>

@yield('script')

</body>
</html>
