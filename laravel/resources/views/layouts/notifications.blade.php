@if(Session::has('message'))
    <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
            <span class="alert-inner--text"><i class="ni ni-air-baloon"></i> {{ Session::get('message') }}</span><br>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(!$errors->isEmpty())
<div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
    @foreach($errors->all() as $error)
        <span class="alert-inner--text"><i class="ni ni-air-baloon"></i> {{ $error }}</span><br>
    @endforeach
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif