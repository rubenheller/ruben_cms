<nav class="navbar navbar-vertical fixed-left navbar-expand-md bg-gradient-secondary" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
                aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand pt-0" href="#">
            <img style="max-height: 4.7rem" src="{{ asset('images/logo.png') }}" class="navbar-brand-img" alt="...">
        </a>
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <div class="media align-items-center">
{{--            <span class="avatar avatar-sm rounded-circle">--}}
{{--              <img alt="Image placeholder"--}}
{{--                   src="{{ asset('argon-dashboard-master/assets/img/theme/team-1-800x800.jpg') }}">--}}
{{--            </span>--}}
                    </div>
                </a>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="#">
                            <img src="{{ asset('images/logo.png') }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                                data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                                aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(Request::is('admin')) active @endif" href="{{ route('admin.home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(Request::is('admin/user*')) active @endif"
                       href="{{ route('admin.user.index') }}">
                        <i class="ni ni-single-02 text-primary"></i> Administrators
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(Request::is('admin/token*')) active @endif"
                       href="{{ route('admin.token.index') }}">
                        <i class="ni ni-send text-primary"></i> Api Tokens
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
