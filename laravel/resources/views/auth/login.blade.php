<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login | {{ env('APP_NAME', 'Laravel') }}</title>
    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('css/main.css') !!}
    <script src="{{ asset('js/app.js') }}" defer></script>
    {!! HTML::script('js/jquery-3.3.1.min.js') !!}
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: #CC0000;
        }

        .login-nav {
            background-color: lightgray;
        }

        .loginmodal-container {
            padding: 30px;
            max-width: 350px;
            width: 100%;
            background-color: #F7F7F7;
            margin: 0 auto;
            border-radius: 2px;
            overflow: hidden;
            height: 100px;
            display: table;
            position: absolute;
            left: 0;
            right: 0;
            top: 25%;
        }

        .loginmodal-container h1 {
            text-align: center;
            font-size: 1.8em;
        }

        .loginmodal-container input[type=submit] {
            width: 100%;
            display: block;
            margin-bottom: 10px;
            position: relative;
        }

        .loginmodal-container input[type=text], input[type=password] {
            height: 44px;
            font-size: 16px;
            width: 100%;
            margin-bottom: 10px;
            -webkit-appearance: none;
            background: #fff;
            border: 1px solid #d9d9d9;
            border-top: 1px solid #c0c0c0;
            /* border-radius: 2px; */
            padding: 0 8px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .loginmodal-container input[type=text]:hover, input[type=password]:hover {
            border: 1px solid #b9b9b9;
            border-top: 1px solid #a0a0a0;
            -moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        }

        .loginmodal {
            text-align: center;
            font-size: 14px;
            font-family: 'Arial', sans-serif;
            font-weight: 700;
            height: 36px;
            padding: 0 8px;
            /* border-radius: 3px; */
            /* -webkit-user-select: none;
              user-select: none; */
        }

        .loginmodal-submit {
            /* border: 1px solid #3079ed; */
            border: 0px;
            color: #fff;
            text-shadow: 0 1px rgba(0, 0, 0, 0.1);
            background-color: #CC0000;
            padding: 17px 0px;
            font-size: 14px;
            /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
        }

        .loginmodal-submit:hover {
            /* border: 1px solid #2f5bb7; */
            border: 0px;
            text-shadow: 0 1px rgba(0, 0, 0, 0.3);
            background-color: #CC0000;
            /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
        }

        .loginmodal-container a {
            text-decoration: none;
            color: #666;
            font-weight: 400;
            text-align: center;
            display: inline-block;
            opacity: 0.6;
            transition: opacity ease 0.5s;
        }

        .login-help {
            font-size: 12px;
        }

        .help-block {
            color: red;
        }

        .login-logo {
            max-height: 4.5rem;
            padding-bottom: 1.3125rem;
            padding-left: 2.6rem;
        }
    </style>

</head>
<body>

<div class="container">
    <div class="loginmodal-container">
{{--        <img src="{{ asset('images/logo.png') }}" class="login-logo">--}}
        <h3>Project Vuilcontainer</h3>
        {{ Form::open(['route' => 'login']) }}
        <input type="text" name="email" placeholder="Email">
        <input type="password" name="password" placeholder="Wachtwoord">
        @if($errors->has('email'))
            <span class="help-block"><strong>Email en/of wachtwoord is onjuist</strong></span>
        @endif
        <input type="submit" name="login" class="login loginmodal-submit" value="Inloggen">
        {{ Form::close() }}

{{--        <div class="login-help">--}}
{{--            <a href="#">Wachtwoord vergeten</a>--}}
{{--        </div>--}}
    </div>
</div>

</body>
</html>
