<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            [
                'name' => 'Ruben Heller',
                'email' => 'ruben.heller@student.hu.nl',
                'password' => '$2y$12$.DG74342.QQimedhzs5E2OLTAgZ8lXxaeSD.74d4IEAQb7J20fyIS'
            ],
        ]);


    }
}
