<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $view = view('cms.user.index');
        $view->title = 'Administrators';
        $view->users = User::all();
        return $view;
    }

    public function create()
    {
        $view = view('cms.user.create');
        $view->title = 'Administrator toevoegen';
        return $view;
    }

    public function store(UserRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        User::create($input);
        return redirect()->route('admin.user.index')->withMessage('Administrator aangemaakt');
    }


    public function edit(User $user)
    {
       $view = view('cms.user.edit');
       $view->user = $user;
       $view->title = 'Administrator bewerken';
       return $view;
    }

    public function update(UserRequest $request, User $user)
    {
        $input = $request->except('user_id');
        $input['password'] = Hash::make($input['password']);
        $user->update($input);
        return redirect()->route('admin.user.edit', $user->id)->withMessage('Administrator is bijgewerkt');
    }

    public function destroy(User $user)
    {
        $name = $user->name;
        $user->delete();
        return redirect()->route('admin.user.index')->withMessage('Administrator '.$name.' is verwijderd');
    }
}
