<?php namespace App\Http\Controllers\Admin;


use App\Container;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $view = view('cms.index');
        $view->title = 'Dashboard';
        $view->containers = Container::orderBy('current_volume', 'DESC')->get();

        return $view;
    }
}
